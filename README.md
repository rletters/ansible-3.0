# RLetters Deployment

**Development on RLetters has been superseded by the Sciveyor project.** Check
out the new home of our deployment scripts here:
<https://codeberg.org/sciveyor/ansible>.

---

First, prepare your own `inventory.yml` file by copying `inventory.example.yml`
and modifying it to suit your own deployment targets.
[That file](./inventory.example.yml) is filled with comments that should be
helpful to figure out what kinds of machines you need and how to get them
configured.

The underlying systems need to be running either CentOS/RHEL 7+, or a
Debian-based distribution (Jessie/15.04 or later), with systemd. Our standard
testing and production deployments are on CentOS 7 at the moment.

Then, call Ansible to install the base software on all of the servers:

```sh
ansible-galaxy install -r requirements.yml
ansible-playbook -i inventory.yml site.yml
```
